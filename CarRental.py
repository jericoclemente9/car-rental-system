customerName = [0]
customerAge = [0]
customerCar = [0]
customerCost = [0]
customerDays = [0]
customerID = [0]
def NewCustomer():
    cars = ['1. Ferrari = $300/day','2. Bentley = $400/day','3. Pontiac = $500/day','4. Toyota = $250/day','5. Prosche = $450/day']
    while True:
        print("**************")
        Name = input("Name: ")
        Age = input("Age: ")
        ageNum = checkNuminStr(Age)
        if ageNum<18:
            print("Customer is below legal age")
            switcher()
        print("Cars:")
        for i in cars:
            print(i)
        chosenCar = input("Enter Number: ")
        if chosenCar == '1':
            print("Car: Ferrari")
            customerCar.append('Ferrari')
            customerCost.append(300)
        elif chosenCar == '2':
            print("Car: Bentley")
            customerCar.append('Bentley')
            customerCost.append(400)
        elif chosenCar == '3':
            print("Car: Pontiac")
            customerCar.append('Pontiac')
            customerCost.append(500)
        elif chosenCar == '4':
            print("Car: Toyota")
            customerCar.append('Toyota')
            customerCost.append(250)
        elif chosenCar == '5':
            print("Car: Porsche")
            customerCar.append('Porsche')
            customerCost.append(450)
        else:
            print("Invalid input")
            switcher()
        days = input("Number of Days: ")
        days_int=checkNuminStr(days)
        ident = input("ID Presented: ")
        customerName.append(Name)
        customerAge.append(ageNum)
        customerDays.append(days_int)
        customerID.append(ident)
        print("Record saved")
        switcher()

def ViewCustomer():
    print("Search for customers")
    print("*****************")
    searchName = input("Name: ")
    for n in customerName:
        if searchName == n:
            customerIndex = customerName.index(searchName)
            print("Age: ", customerAge[customerIndex])
            print("Car: ", customerCar[customerIndex])
            print("Cost per day($): ", customerCost[customerIndex])
            print("Number of days: ", customerDays[customerIndex])
            print("ID Presented: ", customerID[customerIndex])
            print("Total costs: ", customerCost[customerIndex]*customerDays[customerIndex])
            print("*****************")
            switcher()
        elif searchName not in customerName:
            print("Record does not exist")
            switcher()

def ClearCustomer():
    print("Delete existing record")
    customerDelete = input("Name: ")
    for n in customerName:
        if customerDelete == n:
            customerIndex = customerName.index(customerDelete)
            customerName.pop(customerIndex)
            customerAge.pop(customerIndex)
            customerCar.pop(customerIndex)
            customerCost.pop(customerIndex)
            customerDays.pop(customerIndex)
            customerID.pop(customerIndex)
            print("Record deleted")
            switcher()
        elif customerDelete not in customerName:
            print("Record does not exist")
            switcher()

def switcher():
    print("---------------------")
    print("1. New Customer, 2. View Customer, 3. Delete Customer, 4. Exit")
    switch = input("Enter: ")
    if switch=='1':
        NewCustomer()
    elif switch=='2':
        ViewCustomer()
    elif switch=='3':
        ClearCustomer()
    elif switch=='4':
        exit()
    else:
        print("Invalid input")
        switcher()

def checkNuminStr(arg):
    if str.isdigit(arg) == True:
        res_int = int(arg)
        return res_int
    else:
        print("Invalid input")
        switcher()

print("Welcome")
switcher()